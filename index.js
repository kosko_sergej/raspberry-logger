'use strict';

let logger = null;

const defaultConfig = {
  'name': 'logger',
  'streams': [
    {
      'stream': 'stdout',
      'level': 'trace'
    }
  ]
};

module.exports = function (config = {}) {

  if (!logger) {
    const configure = Object.assign({}, defaultConfig, config);
    const bunyanConfig = require('bunyan-config')(configure);
    logger = require('bunyan').createLogger(bunyanConfig);
  }
  return logger;
};